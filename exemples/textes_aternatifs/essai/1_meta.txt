---
author: Prénom Nom
date: 4 février 2042
description: 'Une description de l''ouvrage.'
identifier:
- scheme: 'ISBN-13'
  text: '978-0-0000-1111-0'
lang: fr
publisher: Usine Livre
rights: '© 2018 ABRUPT, CC BY-NC-SA'
subject: 'un, deux, trois'
title: Le titre de votre livre
---


