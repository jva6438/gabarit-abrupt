--- Script lua pour améliorer la typographie des fichiers produits
--- en remplaçant quelques caractères et en ajoutant des espaces insécables
--- afin d'obtenir un meilleur rendu final notamment avec l'utilisation de CSL,
--- l'usage automatique des guillemets en LaTeX ou encore l'ajout de certains exposants

function Inlines (inlines)
  if FORMAT:match 'html' or FORMAT:match 'epub' then
    for i = 1, #inlines do
      if inlines[i].t == 'Str' and inlines[i].text:match("^!.*")
      and inlines[i-1].t == 'Space' then
        inlines[i-1] = pandoc.RawInline('html', '&nbsp;')
        if inlines[i].text:match("^!»") then
          inlines[i] = pandoc.RawInline('html', '!&nbsp;»')
        end
      elseif inlines[i].t == 'Str' and inlines[i].text:match("^?.*")
      and inlines[i-1].t == 'Space' then
        inlines[i-1] = pandoc.RawInline('html', '&nbsp;')
        if inlines[i].text:match("^?»") then
          inlines[i] = pandoc.RawInline('html', '?&nbsp;»')
        end
      elseif inlines[i].t == 'Str' and inlines[i].text:match("^:.*")
      and inlines[i-1].t == 'Space' then
        inlines[i-1] = pandoc.RawInline('html', '&nbsp;')
        if inlines[i].text:match("^:»") then
          inlines[i] = pandoc.RawInline('html', ':&nbsp;»')
        end
      elseif inlines[i].t == 'Str' and inlines[i].text:match("^;.*")
      and inlines[i-1].t == 'Space' then
        inlines[i-1] = pandoc.RawInline('html', '&nbsp;')
        if inlines[i].text:match("^;»") then
          inlines[i] = pandoc.RawInline('html', ';&nbsp;»')
        end
      end
    end
  end
  return inlines
end

function Str(elem)
  if FORMAT:match 'latex' then
    if elem.text == "« " then
      return pandoc.Str "«"
    elseif elem.text == " »" then
      return pandoc.Str "»"
    elseif elem.text == "1er" then
      return {
        pandoc.Str('1'),
        pandoc.RawInline('latex', '\\textsuperscript{er}')
      }
    elseif elem.text == "1re" then
      return {
        pandoc.Str('1'),
        pandoc.RawInline('latex', '\\textsuperscript{re}')
      }
    elseif elem.text:match(".*%de$") then
      local firstpart = string.sub(elem.text, 0, -2)
      return {
        pandoc.Str(firstpart),
        pandoc.RawInline('latex', '\\textsuperscript{e}')
      }
    elseif elem.text:match("^[nN]ᵒ .*") then
      local number = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(number),
        pandoc.RawInline('latex', '\\textsuperscript{o}~'),
        pandoc.Str(lastpart),
      }
    elseif elem.text:match("^[nN]ᵒˢ .*") then
      local number = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(number),
        pandoc.RawInline('latex', '\\textsuperscript{os}~'),
        pandoc.Str(lastpart),
      }
    elseif elem.text:match("^[fF]ᵒ .*") then
      local folio = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(folio),
        pandoc.RawInline('latex', '\\textsuperscript{o}~'),
        pandoc.Str(lastpart),
      }
    elseif elem.text:match("^[fF]ᵒˢ .*") then
      local folio = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(folio),
        pandoc.RawInline('latex', '\\textsuperscript{os}~'),
        pandoc.Str(lastpart),
      }
    else
      return elem
    end
  elseif FORMAT:match 'html' or FORMAT:match 'epub' then
    if elem.text == "1er" then
      return {
        pandoc.Str('1'),
        pandoc.RawInline('html', '<sup>er</sup>')
      }
    elseif elem.text == "1re" then
      return {
        pandoc.Str('1'),
        pandoc.RawInline('html', '<sup>re</sup>')
      }
    elseif elem.text:match(".*%de$") then
      local firstpart = string.sub(elem.text, 0, -2)
      return {
        pandoc.Str(firstpart),
        pandoc.RawInline('html', '<sup>e</sup>')
      }
    elseif elem.text:match("^[nN]ᵒ .*") then
      local number = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(number),
        pandoc.RawInline('html', '<sup>o</sup>&nbsp;'),
        pandoc.Str(lastpart),
      }
    elseif elem.text:match("^[nN]ᵒˢ .*") then
      local number = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(number),
        pandoc.RawInline('html', '<sup>os</sup>&nbsp;'),
        pandoc.Str(lastpart),
      }
    elseif elem.text:match("^[fF]ᵒ .*") then
      local folio = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(folio),
        pandoc.RawInline('html', '<sup>o</sup>&nbsp;'),
        pandoc.Str(lastpart),
      }
    elseif elem.text:match("^[fF]ᵒˢ .*") then
      local folio = string.sub(elem.text, 0, 1)
      local lastpart = string.sub(elem.text, 7, -1)
      return {
        pandoc.Str(folio),
        pandoc.RawInline('html', '<sup>os</sup>&nbsp;'),
        pandoc.Str(lastpart),
      }
    elseif elem.text:match("^«[^ !?:;]+»$") then
      local changement = string.gsub(elem.text, "«", "«&nbsp;")
      changement = string.gsub(changement, "»", "&nbsp;»")
      return {
        pandoc.RawInline('html', changement)
      }
    elseif elem.text:match("^«[^ !?:;]+")
    or elem.text:match("^«$") then
      local changement = string.gsub(elem.text, "«", "«&nbsp;")
      return {
        pandoc.RawInline('html', changement)
      }
    elseif elem.text:match("[^ !?:;]+»$")
    or elem.text:match("^»$")
    or elem.text:match("[^ !?:;]+»%p$") then
      local changement = string.gsub(elem.text, "»", "&nbsp;»")
      return {
        pandoc.RawInline('html', changement)
      }
    else
      return elem
    end
  end
end
