--- Script lua pour pallier une erreur de citeproc avec pandoc qui place
--- deux fois un point final après "Ibid." ou "op. cit." en fin de ligne.
--- L'erreur ne semple plus exister à partir de la version 2.14.2 de pandoc.

function Inlines (inlines)
  for i = 1, #inlines-1 do
    if inlines[i].t == 'Cite'
    and inlines[i].content[1].t == 'Str' and inlines[i].content[1].text == ''
    and inlines[i].content[2].t == 'Emph'
    and inlines[i].content[3].t == 'Str' and inlines[i].content[3].text == ''
    then
       inlines[i+1] = pandoc.Emph {pandoc.Str('')}
    end
  end
return inlines
end

--- function Inlines (inlines)
---   for i = 2, #inlines-1 do
---     if inlines[i].t == 'Emph'
---     and inlines[i+1].t == 'Str' and inlines[i+1].text == ''
---     and inlines[i-1].t == 'Str' and inlines[i-1].text == ''
---     then
---        inlines[i] = pandoc.Emph {pandoc.Str('Ibid')}
---     end
---     if inlines[i].t == 'Emph'
---     and inlines[i+1].t == 'Str' and inlines[i+1].text == ''
---     and inlines[i-1].t == 'Space'
---     then
---        inlines[i] = pandoc.Emph {pandoc.Str('op.\xc2\xa0cit')}
---     end
---   end
--- return inlines
--- end
