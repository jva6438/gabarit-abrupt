---
author: Prénom Nom
date: 4 février 2042
depot: Premier trimestre 2019
description: Une description de l'ouvrage.
description_ouvrage_reseau: Une indication sur la présence de ce livre sur Internet.
details_colophon: Un paragraphe descriptif sur quelques détails de la confection de l'ouvrage, par exemple (voire ne rien mettre, et effacer tout jusqu'au vpsace\* suivant).
identifier:
- scheme: ISBN-13
  text: 978-0-0000-1111-0
imprimerie: Imprimé en Europe
informations_generales: Quelques informations sur le livre, l'auteur, la maison d'édition, etc.
lang: fr
licence: Cet ouvrage est mis à disposition selon les termes de la Licence Creative Commons Attribution --- Pas d'Utilisation Commerciale --- Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0).
lien_colophon: "https://lesitedevotreusinelivre.com/colophon"
lien_informations_generales: "https://lesitedevotreusinelivre.com/informations"
lien_licence: "https://lesitedevotreusinelivre.com/partage"
lien_ouvrage_reseau: "https://lesitedevotreusinelivre.com/la-page-du-livre/"
lieu: Usine Livre, Mars-sur-Moselle
publisher: Usine Livre
rights: © 2018 Usine Livre, CC BY-NC-SA
subject: un, deux, trois
title: Le titre de votre livre
version: 1.0.1
year: 2042
---

